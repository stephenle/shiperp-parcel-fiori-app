sap.ui.define([
	"com/erpis/shiperp/parcel/controller/BaseController",
	"sap/ui/model/json/JSONModel"
], function(BaseController, JSONModel) {
	"use strict";

	return BaseController.extend("com.erpis.shiperp.parcel.controller.App", {

		onInit: function() {
			var oViewModel = new JSONModel({});

			this.setModel(oViewModel, "appView");

			// apply content density mode to root view
			// this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
			
			// set message model
            // var oMessageManager = sap.ui.getCore().getMessageManager();
            // this.getView().setModel(oMessageManager.getMessageModel(), "message");

            // // activate automatic message generation for complete view
            // oMessageManager.registerObject(this.getView(), true);
		}
	});

});