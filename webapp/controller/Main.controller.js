/*global location*/
jQuery.sap.require("com.erpis.shiperp.parcel.common.jquery_hotkeys");
sap.ui.define([
	"com/erpis/shiperp/parcel/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"com/erpis/shiperp/parcel/model/formatter",
	"sap/m/Token",
	"sap/ui/model/Filter",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"com/erpis/shiperp/parcel/common/Utils",
	"com/erpis/shiperp/parcel/common/hotkeyInterface"
], function(BaseController, JSONModel, formatter, Token, Filter, MessageBox, MessageToast, Utils, HotkeyInterface) {
	"use strict";

	return BaseController.extend("com.erpis.shiperp.parcel.controller.Main", {

		_oLogger: jQuery.sap.log.getLogger("com.erpis.shiperp.parcel.controller.Main"),
		formatter: formatter,
		oBundle: null, // i18n bundle class
		// Commonly used controller attributes
		sProfile: "", // Shipping profile
		sStation: "", // Shipping station
		sInputIDs: "", // Input IDs
		sInputType: "", // Input Type
		sObject: "", // Store basic/object value
		sObjectKey: "", // Store basic/object key value
		sCarrier: "", // Current Carrier
		sService: "", // Selected Service ID from rate
		sDefaultUseScale: "", // Default Use Scale,
		iOriginalExtScaleWeight: 0, // The first time read External scale -> store weight return here
		sOriginalExtScaleWeightUnit: "", // The first time read External scale -> store weight unit return here
		sHUWeightChange: "",
		aFreightUnitShippedItems: [], // The list contain all freight Unit Items which already shipped.

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */
		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit: function() {
			this.oInputTypeDeferred = $.Deferred();

			// Set the controller property to be used globally in the controller
			this.oBundle = this.getResourceBundle();

			// Local Model for view
			var oViewModel = new JSONModel({
				Contents: [],
				Freightunits: [],
				HTS: [],
				References: [],
				CarrierList: [],
				ServiceList: [],
				PreviousShipment: {
					carrier: "",
					service: "",
					billing_option: "",
					shipment_date: "",
					weight: "",
					rate: "",
					trackingno: ""
				}
			});
			this.setModel(oViewModel, "local");

			this.getRouter().getRoute("main").attachPatternMatched(this._onObjectMatched, this);

			//*** add checkbox validator
			this.getView().byId("txtId").addValidator(function(args) {
				var text = args.text;
				return new Token({
					key: text,
					text: text
				});
			});

			// Initialize Message Model
			var oJSONModel = new JSONModel({
				aMessages: [],
				messagesLength: 0
			});
			this.setModel(oJSONModel, "messageModel");

			// hot key
			this.aHotKeyHanlders = [{
				keyCombination: "F8",
				control: this.getView(),
				fnHandler: this.handleHotKeyShip,
				hanlder: this
			}];

			HotkeyInterface.getInstance(this.getOwnerComponent()).bindHotKeys(this.aHotKeyHanlders);
		},

		/**
		 * Binds the view to the object path.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched: function(oEvent) {
			this.showBusy();
			this.sStation = oEvent.getParameter("arguments").Station;
			this.sProfile = oEvent.getParameter("arguments").Profile;

			// Register event load for combobox input type
			this.byId("cbInputType").getBinding("items").attachDataReceived(this.onInputTypeLoaded, this);

			// Initialize control visibilities
			// Hide International, Importer, Specific Carrier tab
			this.byId("iconTabInternational").setVisible(false);
			this.byId("iconTabCarrier").setVisible(false);
			this.byId("iconTabImporter").setVisible(false);
			// Hide HTS button
			this.byId("btnHTS").setVisible(false);

			$.when(this.oInputTypeDeferred).done(function() {
				this.hideBusy();
			}.bind(this));
		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */
		handleHotKeyShip: function() {
			var oShipBtn = this.byId("idShipmentBtn");
			if (oShipBtn.getEnabled()) {
				oShipBtn.firePress();
			}
		},

		unBindAllHotKeys: function() {
			HotkeyInterface.getInstance(this.getOwnerComponent()).unBindHotKeys(this.aHotKeyHanlders);
		},
		// When dropdown input type loaded
		onInputTypeLoaded: function() {
			this.oInputTypeDeferred.resolve();
		},

		// When Freight unit table selection changed
		onSelectionChange: function(oEvent) {
			var oTab = oEvent.getSource();
			var aList = oTab.getSelectedItems();
			var bFlag = false;
			var oHU;
			// un select item in case of non mps
			var sMPSStatus = this.getModel("local").getProperty("/basic/carrier_data/mps/mps");
			var sMPSType = this.getModel("local").getProperty("/basic/carrier_data/mps/mpstype");
			if (sMPSStatus === "X" && sMPSType === "02") {
				// return true;
			} else {
				if (aList.length > 1) {
					MessageBox.error(this.oBundle.getText("errorMPSSelectMsg"));
					// select all
					if (aList.length === oTab.getItems().length) {
						oTab.removeSelections();
					} else {
						oEvent.getParameter("listItem").setSelected(false);
					}
					return;
				}
			}

			// Check if any freight unit has tracking number
			for (var i = 0; i < aList.length; i++) {
				oHU = aList[i];
				if (oHU.getBindingContext("local").getObject().trackingnumber !== "") {
					if (oEvent.getParameter("selected")) {
						bFlag = true;
						break;
					}
				}
			}
			if (bFlag) {
				// Unselect Items with Tracking number available
				for (i = 0; i < aList.length; i++) {
					oHU = aList[i];
					if (oHU.getBindingContext("local").getObject().trackingnumber !== "") {
						if (oEvent.getParameter("selected")) {
							oHU.setSelected(false);
						}
					}
				}
				MessageBox.warning(this.oBundle.getText("SelectHUTableActionNotAllowed"));
			}
		},

		// Add Default HU section
		onAddDefaultHU: function() {
			if (!this.oDialogDefaultHU) {
				this.oDialogDefaultHU = sap.ui.xmlfragment("com.erpis.shiperp.parcel.fragment.CreateDefaultHUDialog", this);
				this.getView().addDependent(this.oDialogDefaultHU);
			}
			this.oDialogDefaultHU.open();
		},

		// Add New HU section
		onAddNewHU: function() {
			if (!this.oDialogNewHU) {
				this.oDialogNewHU = sap.ui.xmlfragment("com.erpis.shiperp.parcel.fragment.CreateHUDialog", this);
				this.getView().addDependent(this.oDialogNewHU);
			}
			this.oDialogNewHU.open();
		},

		onCreateHU: function() {
			this._createHU();
		},

		onCloseNewHUDialog: function() {
			this.oDialogNewHU.close();
		},

		// Delete HU section
		onDeleteHU: function() {
			var aSelectedHUs = this.byId("tableHU").getSelectedItems();
			if (aSelectedHUs.length === 0) {
				MessageBox.error(this.oBundle.getText("SelectHUDelete"));
				return;
			}
			if (this._numOfShippedItems(aSelectedHUs) > 0) {
				MessageBox.error(this.oBundle.getText("errorSelectedShippedItemsMsg"));
				return;
			}
			MessageBox.confirm(this.oBundle.getText("confirmDeleteHUMessage"), {
				title: this.oBundle.getText("ConfirmDeletion"),
				actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
				initialFocus: sap.m.MessageBox.Action.YES,
				onClose: function(oAction) {
					if (oAction === MessageBox.Action.YES) {
						this._deleteHUs(false);
					}
				}.bind(this)
			});
		},

		onDeleteAllHU: function() {
			MessageBox.confirm(this.oBundle.getText("confirmDeleteAllHUMessage"), {
				title: this.oBundle.getText("ConfirmDeletion"),
				actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
				initialFocus: sap.m.MessageBox.Action.YES,
				onClose: function(oAction) {
					if (oAction === MessageBox.Action.YES) {
						this._deleteHUs(true);
					}
				}.bind(this)
			});
		},

		// Press on Reference button on Small Parcel tab
		onBtnPressRefChange: function() {
			this._oRefDialog = Utils.getFragment("", "parcel.DialogRefDisplay", this);
			this._oRefDialog.open();
		},

		onCloseRefereneceDialog: function() {
			this._oRefDialog.close();
		},

		// Press on HTS button
		onOpenHTS: function() {
			this._oHTSDialog = Utils.getFragment("", "DialogHTSDisplay", this);
			this._oHTSDialog.open();
		},

		onCloseHTSDialog: function() {
			this._oHTSDialog.close();
		},

		// Enter press on the search text on the header toolbar
		onSubmit: function(oEvent) {
			if (this.byId("cbInputType").getSelectedKey() !== "") {
				this.sInputType = this.byId("cbInputType").getSelectedKey();
				this.byId("cbInputType").setValueState("None");
			} else {
				MessageBox.error(this.oBundle.getText("missingInputType"));
				this.byId("cbInputType").setValueState("Error");
				return;
			}

			if (oEvent.getSource().getTokens().length === 0) {
				MessageBox.error(this.oBundle.getText("missingInputID"));
				this.byId("txtId").setValueState("Error");
				return;
			} else {
				this.byId("txtId").setValueState("None");
			}

			this.sInputIDs = "";
			for (var i = 0; i < oEvent.getSource().getTokens().length; i++) {
				var sTokenValue = oEvent.getSource().getTokens()[i].getText();
				this.sInputIDs += sTokenValue + "|";
			}
			var sPath = "/ShipmentQuerySet";
			this.showBusy();
			this.getModel().read(sPath, {
				filters: [
					new Filter("inputtype", "EQ", this.sInputType),
					new Filter("inputids", "EQ", this.sInputIDs),
					new Filter("profile", "EQ", this.sProfile),
					new Filter("shippingstation", "EQ", this.sStation)
				],
				urlParameters: {
					"$expand": "Freightunits,Freightunits/FreightunitItems,SerialSet,SerialSet/SerialItemSet,HTS,References,CarrierList,ServiceList,Contents"
				},
				success: function(oData) {
					if (oData.results.length !== 0 && oData.results[0].basic.carrier_data.carrier !== "") {
						this.getModel("local").setProperty("/ShipmentQuery", oData.results[0]);
						this.getModel("local").setProperty("/Contents", oData.results[0].Contents.results);
						this.getModel("local").setProperty("/Freightunits", oData.results[0].Freightunits.results);
						this.getModel("local").setProperty("/HTS", oData.results[0].HTS.results);
						this.getModel("local").setProperty("/References", oData.results[0].References.results);
						this.getModel("local").setProperty("/CarrierList", oData.results[0].CarrierList.results);
						this.getModel("local").setProperty("/ServiceList", oData.results[0].ServiceList.results);
						this.getModel("local").setProperty("/basic", oData.results[0].basic);

						// construct serial list
						this.getModel("local").setProperty("/MasterSerialList", oData.results[0].SerialSet.results);

						// Keep the common properties at controller state
						this.sObject = this.getModel("local").getProperty("/basic/hu_object/Object");
						this.sObjectKey = this.getModel("local").getProperty("/basic/hu_object/ObjectKey");
						this.sCarrier = oData.results[0].basic.carrier_data.carrier;

						// Show Message Strip
						this._displayMessageStripHeader();

						// Read default scale weight
						this._getDefaultScaleWeight();

						// Display tabs accordingly
						this._displayTabs();

						// Filter all the available dropdowns
						this._filterAllDropdowns();

						// Disable inputs fields
						this.byId("txtId").setEditable(false);
						this._updateShippingStatus();

						this.byId("tableItem").removeSelections();
						this.byId("tableHU").removeSelections();
					} else {
						MessageBox.warning(this.oBundle.getText("NoDataFound"));
						// Enable inputs fields
						this.byId("txtId").setEditable(true);
					}
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		onOpenHUItemsDialog: function(oEvent) {
			var oObject = oEvent.getSource().getBindingContext("local").getObject();
			this.getModel("local").setProperty("/FreightunitItems", oObject.FreightunitItems.results);
			if (!this._oHUItemsDialog) {
				this._oHUItemsDialog = sap.ui.xmlfragment("com.erpis.shiperp.parcel.fragment.packing.HUItemsDialog", this);
				this.getView().addDependent(this._oHUItemsDialog);
			}
			this._oHUItemsDialog.open();
		},

		onCloseHUItemsDialog: function() {
			this._oHUItemsDialog.close();
		},

		onSearchMaterial: function() {
			var oFilter = new Filter("material", "Contains", this.byId("txtMaterial").getValue());
			this.byId("tableItem").getBinding("items").filter(oFilter);
		},

		onValidateNumber: function(oEvent) {
			var sNewValue = oEvent.getParameter("newValue");
			var sBalance = oEvent.getSource().getBindingContext("local").getObject().openqty;
			if (parseInt(sNewValue) > parseInt(sBalance)) {
				oEvent.getSource().setValueState("Error");
				MessageBox.error(this.oBundle.getText("partialQuantityError"));
			} else {
				oEvent.getSource().setValueState("None");
			}
		},

		onUpdateDomestic: function(oEvent) {
			var oControl = oEvent.getSource();
			var sCountryCode = oControl.getSelectedKey();
			this.showBusy();
			var sRegionId = oControl.data("nextUpdate");
			this.getModel().callFunction("/GetDomesticFlag", {
				"method": "GET",
				urlParameters: {
					Carrier: this.sCarrier,
					FromCountry: this.getModel("local").getProperty("/basic/partners/shipfrom/address/country"),
					FromState: this.getModel("local").getProperty("/basic/partners/shipfrom/address/state"),
					ToCountry: this.getModel("local").getProperty("/basic/partners/shipto/address/country"),
					ToState: this.getModel("local").getProperty("/basic/partners/shipto/address/state")
				},
				success: function(oData) {
					this.getModel("local").setProperty("/basic/shipment_flags/domestic", oData.GetDomesticFlag.flag);
					this._displayTabs();
					if (sRegionId) {
						this._updateRegion(sRegionId, sCountryCode);
					}
					this.hideBusy();
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		// Change Carrier event
		onCarrierChange: function(oEvent) {
			this.sCarrier = oEvent.getSource().getSelectedKey();
			// Update basic node from new carrier
			this._changeCarrier("");
		},

		onChangeReturnService: function() {
			this._changeReturnService();
		},

		// Reset button click
		onResetData: function() {
			this.byId("txtId").removeAllTokens();
			this.byId("iconTabCarrier").setVisible(false);
			this.byId("iconTabInternational").setVisible(false);
			this.byId("btnHTS").setVisible(false);
			this.byId("iconTabImporter").setVisible(false);
			this.getModel("local").setData({});
			// Enable inputs fields
			this.byId("txtId").setEditable(true);
		},

		// Single rate/Shop rate/Optimization section
		onSingleRateClick: function() {
			var sMPSStatus = this.getModel("local").getProperty("/basic/carrier_data/mps/mps");
			var aSelectedHUs = this.byId("tableHU").getSelectedItems();
			var sMPSType = this.getModel("local").getProperty("/basic/carrier_data/mps/mpstype");
			if (sMPSStatus === "X" && sMPSType === "02") {
				// return true;
			} else {
				if (aSelectedHUs.length !== 1) {
					MessageBox.error(this.oBundle.getText("errorMPSSelectMsg"));
					return;
				}
			}
			this._getFreightCost("S");
		},

		onRateShopClick: function() {
			var sMPSStatus = this.getModel("local").getProperty("/basic/carrier_data/mps/mps");
			var aSelectedHUs = this.byId("tableHU").getSelectedItems();
			var sMPSType = this.getModel("local").getProperty("/basic/carrier_data/mps/mpstype");
			if (sMPSStatus === "X" && sMPSType === "02") {
				// return true;
			} else {
				if (aSelectedHUs.length !== 1) {
					MessageBox.error(this.oBundle.getText("errorMPSSelectMsg"));
					return;
				}
			}
			this._getFreightCost("R");
		},

		onOptimizationClick: function() {
			var sMPSStatus = this.getModel("local").getProperty("/basic/carrier_data/mps/mps");
			var aSelectedHUs = this.byId("tableHU").getSelectedItems();
			var sMPSType = this.getModel("local").getProperty("/basic/carrier_data/mps/mpstype");
			if (sMPSStatus === "X" && sMPSType === "02") {
				// return true;
			} else {
				if (aSelectedHUs.length !== 1) {
					MessageBox.error(this.oBundle.getText("errorMPSSelectMsg"));
					return;
				}
			}
			this._getFreightCost("O");
		},

		onCloseRateDialog: function() {
			this.oRateDialog.close();
		},

		onRateSelected: function() {
			if (sap.ui.getCore().byId("tableRates").getSelectedItem() == null) {
				MessageBox.error(this.oBundle.getText("SelectItemToContinue"));
				return;
			}
			var oRate = sap.ui.getCore().byId("tableRates").getSelectedItem().getBindingContext("local").getObject();
			this.getModel("local").setProperty("/basic/carrier_data/carrier", oRate.carrierid);
			this.sCarrier = oRate.carrierid;
			this.sService = oRate.serviceid;
			this.oRateDialog.close();
			this.byId("iconTabBarMain").setSelectedKey("tab_parcel");

			// Update basic node from new carrier
			this._changeCarrier((oRate.serviceid) ? oRate.serviceid : "");
		},

		// Pack and Unpack section
		onPackPartial: function() {
			var aSelectedItems = this.byId("tableItem").getSelectedItems();
			if (aSelectedItems.length !== 1) {
				MessageBox.error(this.oBundle.getText("SelectItemPackPartial"));
				return;
			}
			var aSelectedHUs = this.byId("tableHU").getSelectedItems();
			if (aSelectedHUs.length !== 1) {
				MessageBox.error(this.oBundle.getText("SelectHUPack"));
				return;
			}
			this._packPartial();
		},

		onPackMaterial: function() {
			var aSelectedItems = this.byId("tableItem").getSelectedItems();
			if (aSelectedItems.length === 0) {
				MessageBox.error(this.oBundle.getText("SelectItemPack"));
				return;
			}
			var aSelectedHUs = this.byId("tableHU").getSelectedItems();
			if (aSelectedHUs.length !== 1) {
				MessageBox.error(this.oBundle.getText("SelectHUPack"));
				return;
			}

			this._packMaterial();
		},

		onUnpack: function() {
			var aSelectedHUs = this.byId("tableHU").getSelectedItems();
			if (aSelectedHUs.length === 0) {
				MessageBox.error(this.oBundle.getText("SelectHUUnPack"));
				return;
			}
			if (this._numOfShippedItems(aSelectedHUs) > 0) {
				MessageBox.error(this.oBundle.getText("errorSelectedShippedItemsMsg"));
				return;
			}
			MessageBox.confirm(this.oBundle.getText("confirmUnPackHUMessage"), {
				title: this.oBundle.getText("ConfirmUnPacking"),
				actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
				initialFocus: sap.m.MessageBox.Action.YES,
				onClose: function(oAction) {
					if (oAction === MessageBox.Action.YES) {
						this._unpack();
					}
				}.bind(this)
			});
		},

		onExecute: function() {
			var sMPSStatus = this.getModel("local").getProperty("/basic/carrier_data/mps/mps");
			var aSelectedHUs = this.byId("tableHU").getSelectedItems();
			var sMPSType = this.getModel("local").getProperty("/basic/carrier_data/mps/mpstype");
			if (sMPSStatus === "X" && sMPSType === "02") {
				// return true;
			} else {
				if (aSelectedHUs.length !== 1) {
					MessageBox.error(this.oBundle.getText("errorMPSSelectMsg"));
					return;
				}
			}

			MessageBox.confirm(this.oBundle.getText("configmshipmentMsg"), {
				title: this.oBundle.getText("shipTitle"),
				actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
				initialFocus: sap.m.MessageBox.Action.YES,
				onClose: function(oAction) {
					if (oAction === MessageBox.Action.YES) {
						this._execute();
					}
				}.bind(this)
			});
		},

		onChangeScaleOption: function(oEvent) {
			if (oEvent.getSource().getSelectedKey() === "0001") {
				var aSelectedHUs = this.byId("tableHU").getItems();
				this.showBusy();
				this.getModel().callFunction("/GetExternalScale", {
					urlParameters: {
						ShippingStation: this.sStation
					},
					success: function(oData) {
						if (oData.GetExternalScale.Weight && oData.GetExternalScale.WeightUnit) {
							var aHUs = this.getModel("local").getProperty("/Freightunits");
							for (var i = 0; i < aSelectedHUs.length; i++) {
								var oObject = aSelectedHUs[i].getBindingContext("local").getObject();
								for (var j = 0; j < aHUs.length; j++) {
									if (aHUs[j].freightunitkey === oObject.freightunitkey) {
										aHUs[j].weight = parseFloat(oData.GetExternalScale.Weight).toFixed(2);
										this.iOriginalExtScaleWeight = aHUs[j].weight;
										aHUs[j].weight_unit = oData.GetExternalScale.WeightUnit;
										this.sOriginalExtScaleWeightUnit = oData.GetExternalScale.WeightUnit;
									}
								}
							}
							this.getModel("local").setProperty("/Freightunits", aHUs);
						}
						this.hideBusy();
					}.bind(this),
					error: function(oError) {
						this._handleODataError(oError);
						this.hideBusy();
					}.bind(this)
				});
			} else if (oEvent.getSource().getSelectedKey() === "0002") {
				this._getContentAndHUTable("All");
			}
		},

		onNAFTAPress: function() {
			this.oNaftaDialog = Utils.getFragment("", "NAFTADialog", this);
			this.oNaftaDialog.open();
		},

		onCloseNAFTAialog: function() {
			this.oNaftaDialog.close();
		},

		onGoNextLineItem: function(oEvent) {
			var oControl = oEvent.getSource();
			var oSelectedItem = oControl.getParent().getParent();

			var oTable = this.getView().byId("tableHU");
			var oItems = oTable.getItems();
			var oNextItemSelected = null;
			for (var i = 0; i < oItems.length; i++) {
				// check if exist next item or not.
				if (oSelectedItem === oItems[i] && (i + 1) <= (oItems.length - 1)) {
					oNextItemSelected = oItems[i + 1];
					break;
				}
			}
			if (!oNextItemSelected) {
				return;
			}
			var oFirstInput = oNextItemSelected.getCells()[3].getItems()[0];
			if (oFirstInput.getVisible() && oFirstInput.getEditable()) {
				oFirstInput.focus();
			}
		},

		onSerialPress: function() {
			if (!this.oSerialDialog) {
				this.oSerialDialog = sap.ui.xmlfragment("com.erpis.shiperp.parcel.fragment.SerialDialog", this);
				this.getView().addDependent(this.oSerialDialog);
			}
			this.oSerialDialog.open();
			this.aFreightUnitShippedItems = this._getShippedItems();
			var oCarousel = sap.ui.getCore().byId("serialCarousel");
			var oPages = oCarousel.getPages();
			if (oPages.length > 0) {
				oCarousel.setActivePage(oPages[0]);
				oCarousel.firePageChanged({
					newActivePageId: oPages[0].getId()
				});
			}
		},

		onSerialItemChange: function(oEvent) {
			var sActivePageId = oEvent.getParameter("newActivePageId");
			var oActivePage = sap.ui.getCore().byId(sActivePageId);
			var oData = oActivePage.getBindingContext("local").getObject();
			this._displaySerialList(oData);
			var bEditable = true;
			this.aFreightUnitShippedItems.forEach(function(item) {
				if (item.material === oData.matnr && item.delivery === oData.vbeln) {
					bEditable = false;
					return;
				}
			});

			this.getModel("local").setProperty("/bSerialItemEditable", bEditable);
		},

		onChangeSerialMultiInput: function(oEvent) {
			var oControl = oEvent.getSource();
			oControl.addValidator(function(args) {
				var text = args.text;
				var aSerialList = this.getModel("local").getProperty("/SelectedSerial/SerialItemSet/results");
				for (var i = 0; i < aSerialList.length; i++) {
					if (aSerialList[i].SERNR === text) {
						return;
					}
				}
				for (i = 0; i < aSerialList.length; i++) {
					if (aSerialList[i].SERNR === "") {
						aSerialList[i].SERNR = text;
						break;
					}
				}
				this.getModel("local").setProperty("/SelectedSerial/SerialItemSet/results", aSerialList);
				oControl.setValue("");
			}.bind(this));
			oControl.attachEventOnce("change", this.onChangeSerialMultiInput, this);
		},

		onClearSerials: function() {
			var aSerialList = this.getModel("local").getProperty("/SelectedSerial/SerialItemSet/results");
			for (var i = 0; i < aSerialList.length; i++) {
				aSerialList[i].SERNR = "";
			}
			this.getModel("local").setProperty("/SelectedSerial/SerialItemSet/results", aSerialList);
		},

		onCloseSerialDialog: function() {
			// var aMasterList = this.getModel("local").getProperty("/MasterSerialList");
			// var i;
			// for (i = 0; i < aMasterList.length; i++) {
			// 	if (oItem.delivery === aMasterList[i].vbeln && oItem.deliveryitem === aMasterList[i].posnr) {
			// 		// clone original serial item set
			// 		aMasterList[i].SerialListItem = this.aOriginialSerialItemSet;
			// 		break;
			// 	}
			// }
			this.oSerialDialog.close();
		},

		onPostSerialsPress: function() {
			var oRequestData = this._generatePostSerialsUsecase();
			this.showBusy();
			this.getModel().create("/ShipmentQuerySet", oRequestData, {
				success: function(oData) {
					// Update serial list
					this.getModel("local").setProperty("/MasterSerialList", oData.SerialSet.results);
					MessageToast.show(this.oBundle.getText("serialpostsuccess"));
					this.hideBusy();
					this.oSerialDialog.close();
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		onChangeDeliverySerial: function(oEvent) {
			var oControl = oEvent.getSource();
			var sSelectedDelivery = oControl.getValue();
			this._displaySerialList(sSelectedDelivery);
		},

		onValidateAddressPress: function() {
			var oRequestData = this._generateValidateAddressUsecase();
			this.showBusy();
			this.getModel().create("/ShipmentQuerySet", oRequestData, {
				success: function(oData) {
					var oNewAddress = oData.basic.partners.shipto;
					this.getModel("local").setProperty("/NewAddress", oNewAddress);
					this.oValidateAdressDialog = Utils.getFragment("", "ValidateAddressDialog", this);
					this.oValidateAdressDialog.open();
					this.hideBusy();
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		onValidateEmailDialogClose: function() {
			this.oValidateAdressDialog.close();
		},

		onValidateAddressConfirmPress: function() {
			var oNewAddress = this.getModel("local").getProperty("/NewAddress");
			if (this.getModel("local").getProperty("/NewAddress/address/name1") === "" ||
				this.getModel("local").getProperty("/NewAddress/address/street1") === "" ||
				this.getModel("local").getProperty("/NewAddress/address/telephone") === "") {
				return;
			} else {
				// update address
				this.getModel("local").setProperty("/basic/partners/shipto", oNewAddress);
			}
		},

		onEditItemPackingPress: function(oEvent) {
			var bEditable = oEvent.getSource().getPressed();
			if (bEditable) {
				if (this.getModel("local").getProperty("/UseScale") === "0001") {
					// Make column length editable
					oEvent.getSource().getParent().getParent().getCells()[5].getItems()[0].setVisible(true);
					oEvent.getSource().getParent().getParent().getCells()[5].getItems()[1].setVisible(false);
					// Make column width editable
					oEvent.getSource().getParent().getParent().getCells()[6].getItems()[0].setVisible(true);
					oEvent.getSource().getParent().getParent().getCells()[6].getItems()[1].setVisible(false);
					// Make column height editable
					oEvent.getSource().getParent().getParent().getCells()[7].getItems()[0].setVisible(true);
					oEvent.getSource().getParent().getParent().getCells()[7].getItems()[1].setVisible(false);
					// Make column UoM editable
					oEvent.getSource().getParent().getParent().getCells()[8].getItems()[0].setVisible(true);
					oEvent.getSource().getParent().getParent().getCells()[8].getItems()[1].setVisible(false);

				} else if (this.getModel("local").getProperty("/UseScale") === "0002") {
					// Make column weight editable
					oEvent.getSource().getParent().getParent().getCells()[3].getItems()[0].setVisible(true);
					oEvent.getSource().getParent().getParent().getCells()[3].getItems()[1].setVisible(false);
					// Make column length editable
					oEvent.getSource().getParent().getParent().getCells()[5].getItems()[0].setVisible(true);
					oEvent.getSource().getParent().getParent().getCells()[5].getItems()[1].setVisible(false);
					// Make column width editable
					oEvent.getSource().getParent().getParent().getCells()[6].getItems()[0].setVisible(true);
					oEvent.getSource().getParent().getParent().getCells()[6].getItems()[1].setVisible(false);
					// Make column height editable
					oEvent.getSource().getParent().getParent().getCells()[7].getItems()[0].setVisible(true);
					oEvent.getSource().getParent().getParent().getCells()[7].getItems()[1].setVisible(false);
				}
			} else {
				// Make column weight uneditable
				oEvent.getSource().getParent().getParent().getCells()[3].getItems()[0].setVisible(false);
				oEvent.getSource().getParent().getParent().getCells()[3].getItems()[1].setVisible(true);
				// Make column length uneditable
				oEvent.getSource().getParent().getParent().getCells()[5].getItems()[0].setVisible(false);
				oEvent.getSource().getParent().getParent().getCells()[5].getItems()[1].setVisible(true);
				// Make column width uneditable
				oEvent.getSource().getParent().getParent().getCells()[6].getItems()[0].setVisible(false);
				oEvent.getSource().getParent().getParent().getCells()[6].getItems()[1].setVisible(true);
				// Make column height uneditable
				oEvent.getSource().getParent().getParent().getCells()[7].getItems()[0].setVisible(false);
				oEvent.getSource().getParent().getParent().getCells()[7].getItems()[1].setVisible(true);
				// Make column UoM uneditable
				oEvent.getSource().getParent().getParent().getCells()[8].getItems()[0].setVisible(false);
				oEvent.getSource().getParent().getParent().getCells()[8].getItems()[1].setVisible(true);
			}
		},

		onHUTabUpdateFinished: function(oEvent) {
			if (this.sHUWeightChange !== "") {
				var aHUs = oEvent.getSource().getItems();
				var oObject;
				var oButton;
				for (var j = 0; j < aHUs.length; j++) {
					oObject = aHUs[j].getBindingContext("local").getObject();
					if (oObject.freightunitkey === this.sHUWeightChange) {
						oButton = aHUs[j].getCells()[aHUs[j].getCells().length - 1].getItems()[1];
						oButton.setPressed(!oButton.getPressed());
						this.sHUWeightChange = "";
						break;
					}
				}
			}
		},

		onReupdateItemScalePress: function(oEvent) {
			var oObject = oEvent.getSource().getBindingContext("local").getObject();
			var sWeight = oObject.weight;
			if (oEvent.getSource().getPressed()) {
				this.showBusy();
				this.getModel().callFunction("/GetExternalScale", {
					urlParameters: {
						ShippingStation: this.sStation
					},
					success: function(oData) {
						if (oData.GetExternalScale.Weight && oData.GetExternalScale.WeightUnit) {
							var aHUs = this.getModel("local").getProperty("/Freightunits");
							for (var j = 0; j < aHUs.length; j++) {
								if (aHUs[j].freightunitkey === oObject.freightunitkey) {
									if (parseFloat(sWeight) === parseFloat(oData.GetExternalScale.Weight)) {
										this.sHUWeightChange = "";
									} else {
										this.sHUWeightChange = oObject.freightunitkey;
										aHUs[j].weight = parseFloat(oData.GetExternalScale.Weight).toFixed(2);
										aHUs[j].weight_unit = oData.GetExternalScale.WeightUnit;
									}
									break;
								}
							}
							this.getModel("local").setProperty("/Freightunits", aHUs);
						}
						this.hideBusy();
					}.bind(this),
					error: function(oError) {
						this._handleODataError(oError);
						this.hideBusy();
					}.bind(this)
				});
			} else {
				var aHUs = this.getModel("local").getProperty("/Freightunits");
				for (var j = 0; j < aHUs.length; j++) {
					if (aHUs[j].freightunitkey === oObject.freightunitkey) {
						aHUs[j].weight = parseFloat(this.iOriginalExtScaleWeight).toFixed(2);
						aHUs[j].weight_unit = this.sOriginalExtScaleWeightUnit;
					}
				}
				this.getModel("local").setProperty("/Freightunits", aHUs);
			}
		},

		onShowTrackingNumberPress: function(oEvent) {
			var oControl = oEvent.getSource();
			var oData = oControl.getBindingContext("local").getObject();
			var sTrackingNo = oData.trackingnumber;
			var oPopover = new sap.m.Popover({
				showHeader: false,
				content: [
					new sap.m.Text({
						text: sTrackingNo
					}).addStyleClass("sapUiTinyMargin")
				]
			});
			oPopover.openBy(oControl);
		},

		onHazardousPress: function() {
			var aSelectedHUs = this.byId("tableHU").getSelectedItems();
			if (aSelectedHUs.length === 0) {
				MessageBox.error(this.oBundle.getText("hazmatselectedMsgError"));
				return;
			}
			if (aSelectedHUs.length > 1) {
				MessageBox.error(this.oBundle.getText("hazmatselectedMsg"));
				return;
			}
			this._getHazardous();
		},

		onHazardousClose: function() {
			this.oHazmatDialog.close();
		},

		onHazmatUpdatePress: function() {
			var oBinding = this.byId(this.getView().createId("hazmatDetailContainer")).getBindingContext("local");
			if (!oBinding) {
				// no data selected
				return;
			}
			this._validateHazmat();
		},

		onHazmatContinuePress: function() {
			this._validateAllHazmat();
		},

		onHazmatSelectedChangePress: function(oEvent) {
			var oControl = oEvent.getSource();
			var oBinding = oControl.getBindingContext("local");
			var oData = oBinding.getObject();
			if (oControl.getSelected()) {
				oData.Selected = "X";
			} else {
				oData.Selected = "";
			}
			this.getModel("local").setProperty(oBinding.getPath(), oData);
		},

		onDotIDNumberLinkPress: function(oEvent) {
			var oControl = oEvent.getSource();
			var oView = this.getView();
			var oBindingData = oControl.getBindingContext("local").getObject();

			if (oBindingData.Selected !== "X") {
				MessageBox.information(this.oBundle.getText("hazmatDotIdNotSelectedMsg"));
				return;
			}

			var oDetailContainer = this.byId(oView.createId("hazmatDetailContainer"));
			var sPath = "local>" + oControl.getBindingContext("local").getPath();
			this._updatePackingInstr(oControl.getBindingContext("local"));
			oDetailContainer.bindElement(sPath);
		},

		onAddHazmatShowDialogPress: function() {
			var oNewHazmat = {
				Dotidnumber: ""
			};
			this.getModel("local").setProperty("/NewHazMatItem", oNewHazmat);
			this.oHazmatAddItemDialog = Utils.getFragment("", "AddHazmatDialog", this);
			this.oHazmatAddItemDialog.open();
		},
		onAddHazmatClose: function() {
			this.oHazmatAddItemDialog.close();
		},
		onHazmatAddItemPress: function() {
			var oNewHazmat = this.getModel("local").getProperty("/NewHazMatItem");
			var aFreightunitHazmats = this.getModel("local").getProperty("/FreightunitHazmats");
			var oFreightUnit = this.byId("tableHU").getSelectedItem().getBindingContext("local").getObject();
			oNewHazmat.freightunitkey = oFreightUnit.freightunitkey;
			oNewHazmat.hu_id = oFreightUnit.hu_id;
			oNewHazmat.linenumber = aFreightunitHazmats.length + 1;
			oNewHazmat.shipmentid = "";

			aFreightunitHazmats.push(oNewHazmat);
			this.getModel("local").setProperty("/FreightunitHazmats", aFreightunitHazmats);
			this.oHazmatAddItemDialog.close();
		},

		onPCDGunCBChange: function(oEvent) {
			var oControl = oEvent.getSource();
			var sKey = oControl.getSelectedKey();
			if (sKey === "") {
				return;
			}
			sKey = oControl.getSelectedItem().getBindingContext().getObject().tkui + sKey;
			this.showBusy();
			this.getModel().callFunction("/GetSingleHazmat", {
				urlParameters: {
					HazmatID: sKey
				},
				success: function(oData) {
					oData.Dotidnumber = sKey;
					this.getModel("local").setProperty("/NewHazMatItem", oData);
					this.hideBusy();
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		onAuthorizationChange: function(oEvent) {
			var oControl = oEvent.getSource();
			this._updatePackingInstr(oControl.getBindingContext("local"));
		},

		onCalculatResult: function(oEvent) {
			var oBinding = oEvent.getSource().getBindingContext("local");
			var oObject = oBinding.getObject();
			var dResult = 0;
			try {
				if (oObject.Actualqty === "") {
					oObject.Actualqty = 0;
				}
				if (oObject.Innercontainer === "") {
					oObject.Innercontainer = 0;
				}

				dResult = parseFloat(oObject.Actualqty, 10) * parseFloat(oObject.Innercontainer, 10);
			} catch (ex) {
				// 
			}
			oObject.Result = dResult.toFixed(3);
			this.getModel("local").setProperty(oBinding.getPath(), oObject);
		},

		onPackMatValueRequested: function() {
			this.oPackMatDlg = Utils.getFragment("", "PackMatDialog", this);
			this.oPackMatDlg.open();
		},

		handlePackMatValueHelpClose: function() {
			this.oPackMatDlg.close();
		},

		handlePackMatValueHelpSearch: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilterID = new Filter("Matnr", sap.ui.model.FilterOperator.Contains, sValue);
			var oFilterValue = new Filter("Maktx", sap.ui.model.FilterOperator.Contains, sValue);
			var oAllFilter = new Filter([oFilterID, oFilterValue], false);

			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([oAllFilter]);
		},

		onPackMatConfirm: function(oEvent) {

			var oSelectedItem = oEvent.getParameter("selectedItem");
			if (oSelectedItem) {
				var oPackMat = sap.ui.getCore().byId("txtPackMat");
				oPackMat.setValue(oSelectedItem.getTitle());
			}
			oEvent.getSource().getBinding("items").filter([]);
		},

		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */
		_getFreightCost: function(sWhichAction) {
			var oRequestData = this._generateGetFreightCostUsecase(sWhichAction);
			this.showBusy();
			this.getModel().create("/ShipmentQuerySet", oRequestData, {
				success: function(oData) {
					try {
						this.getModel("local").setProperty("/Rates", oData.CarrierRates.results);
					} catch (exc) {
						this._oLogger.info("No Carrier Rates");
					}
					try {
						this.getModel("local").setProperty("/RateErrors", oData.CarrierRateErrors.results);
					} catch (exc) {
						this._oLogger.info("No Carrier Rates");
					}
					if (!this.oRateDialog) {
						this.oRateDialog = sap.ui.xmlfragment("com.erpis.shiperp.parcel.fragment.RatesDialog", this);
						this.getView().addDependent(this.oRateDialog);
					}
					this.oRateDialog.open();
					this.hideBusy();
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		_generateGetFreightCostUsecase: function(sWhichAction) {
			var sActionName = "GetSingleFreightCost";
			if (sWhichAction === "S") {
				sActionName = "GetSingleFreightCost";
			} else if (sWhichAction === "R") {
				sActionName = "GetShopFreightCost";
			} else if (sWhichAction === "O") {
				sActionName = "GetOptimizationFreightCost";
			} else {
				sActionName = "GetSingleFreightCost";
			}
			var aFreightunits = this._getSelectedFreightUnits();
			var oData = {
				shipmentid: "",
				inputids: (this.sInputIDs) ? this.sInputIDs : "",
				inputtype: (this.sInputType) ? this.sInputType : "",
				profile: (this.sProfile) ? this.sProfile : "",
				shippingstation: (this.sStation) ? this.sStation : "",
				action: sActionName,
				basic: this.getModel("local").getProperty("/basic"),
				Freightunits: aFreightunits,
				HTS: this.getModel("local").getProperty("/HTS"),
				References: this.getModel("local").getProperty("/References"),
				CarrierRates: [],
				CarrierRateErrors: []
			};
			return oData;
		},

		_getContentAndHUTable: function(sWhichTable) {
			var oRequestData = this._generateGetContentHUUsecase(sWhichTable);
			this.showBusy();
			this.getModel().create("/ShipmentQuerySet", oRequestData, {
				success: function(oData) {
					if (oData.Contents) {
						this.getModel("local").setProperty("/Contents", oData.Contents.results);
					} else {
						this.getModel("local").setProperty("/Contents", []);
					}
					if (oData.Freightunits) {
						for (var i = 0; i < oData.Freightunits.results.length; i++) {
							if (!oData.Freightunits.results[i].FreightunitItems) {
								oData.Freightunits.results[i].FreightunitItems = {};
							}
							if (oData.Freightunits.results[i].FreightunitItems.results == null) {
								oData.Freightunits.results[i].FreightunitItems = [];
							}
						}
						this.getModel("local").setProperty("/Freightunits", oData.Freightunits.results);
					} else {
						this.getModel("local").setProperty("/Freightunits", []);
					}
					this.byId("tableItem").removeSelections();
					this.byId("tableHU").removeSelections();
					// update Ship Button enable.
					this._updateShippingStatus();
					// select next HU item if execute successfully.
					this._selectNextItem();
					this.hideBusy();
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		_generateGetContentHUUsecase: function(sWhichTable) {
			var sActionName = "GetContentAndHUTable";
			if (sWhichTable === "Content") {
				sActionName = "GetContentTable";
			} else if (sWhichTable === "HU") {
				sActionName = "GetHUTable";
			} else if (sWhichTable === "All") {
				sActionName = "GetContentAndHUTable";
			} else {
				sActionName = "GetContentAndHUTable";
			}

			var sMPSStatus = this.getModel("local").getProperty("/basic/carrier_data/mps/mps");
			var aFreightUnits = [];
			var sMPSType = this.getModel("local").getProperty("/basic/carrier_data/mps/mpstype");
			if (sMPSStatus === "X" && sMPSType === "02") {
				aFreightUnits = this.getModel("local").getProperty("/Freightunits");
			} else {
				aFreightUnits = this._getSelectedFreightUnits();
			}

			for (var i = 0; i < aFreightUnits.length; i++) {
				if (aFreightUnits[i].FreightunitHazmat == null) {
					aFreightUnits[i].FreightunitHazmat = [];
				}
			}

			var oData = {
				shipmentid: this.getModel("local").getProperty("/UseScale"),
				inputids: (this.sInputIDs) ? this.sInputIDs : "",
				inputtype: (this.sInputType) ? this.sInputType : "",
				profile: (this.sProfile) ? this.sProfile : "",
				shippingstation: (this.sStation) ? this.sStation : "",
				action: sActionName,
				basic: this.getModel("local").getProperty("/basic"),
				Freightunits: aFreightUnits,
				Contents: this.getModel("local").getProperty("/Contents"),
				HTS: this.getModel("local").getProperty("/HTS"),
				References: this.getModel("local").getProperty("/References"),
				CarrierRates: []
			};
			oData.Freightunits = JSON.parse(JSON.stringify(oData.Freightunits));
			oData.Contents = JSON.parse(JSON.stringify(oData.Contents));
			return oData;
		},

		_changeCarrier: function(sServiceID) {
			// Update Carrier name and service id
			var sCarrierName = this.byId("cbCarrier").getSelectedItem().getText();
			this.getModel("local").setProperty("/basic/carrier_data/CarrierName", sCarrierName);
			this.getModel("local").setProperty("/basic/carrier_data/service", sServiceID);
			// Filter all the available dropdowns
			this._filterAllDropdowns();
			var oRequestData = this._generateChangeCarrierUsecase();
			this.showBusy();
			this.getModel().create("/ShipmentQuerySet", oRequestData, {
				success: function(oData) {
					if (oData.basic) {
						oData.basic.hu_object.Object = this.sObject;
						oData.basic.hu_object.ObjectKey = this.sObjectKey;
						this.getModel("local").setProperty("/basic", oData.basic);
					}
					if (oData.References) {
						this.getModel("local").setProperty("/References", oData.References.results);
					} else {
						this.getModel("local").setProperty("/References", []);
					}
					// display message strip header.
					this._displayMessageStripHeader();

					// used to reupdate service id when selecting rate entry from the rate dialog
					if (this.sService !== "") {
						this.getModel("local").setProperty("/basic/carrier_data/service", this.sService);
						this.sService = "";
					}

					this.hideBusy();
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		_generateChangeCarrierUsecase: function() {
			var oData = {
				shipmentid: "",
				inputids: (this.sInputIDs) ? this.sInputIDs : "",
				inputtype: (this.sInputType) ? this.sInputType : "",
				profile: (this.sProfile) ? this.sProfile : "",
				shippingstation: (this.sStation) ? this.sStation : "",
				action: "ChangeCarrier",
				basic: this.getModel("local").getProperty("/basic"),
				References: this.getModel("local").getProperty("/References")
			};
			return oData;
		},

		_changeReturnService: function() {
			var oRequestData = this._generateChangeReturnServiceUsecase();
			this.showBusy();
			this.getModel().create("/ShipmentQuerySet", oRequestData, {
				success: function(oData) {
					if (oData.basic.partners) {
						this.getModel("local").setProperty("/basic/partners/shipfrom/address", oData.basic.partners.shipfrom.address);
						this.getModel("local").setProperty("/basic/partners/shipto/address", oData.basic.partners.shipto.address);
						this.getModel("local").setProperty("/basic/shipment_flags/addr_switch", oData.basic.shipment_flags.addr_switch);
					}
					this._filterAllDropdowns();
					this.hideBusy();
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		_generateChangeReturnServiceUsecase: function() {
			var oData = {
				shipmentid: "",
				inputids: (this.sInputIDs) ? this.sInputIDs : "",
				inputtype: (this.sInputType) ? this.sInputType : "",
				profile: (this.sProfile) ? this.sProfile : "",
				shippingstation: (this.sStation) ? this.sStation : "",
				action: "ChangeReturnService",
				basic: this.getModel("local").getProperty("/basic"),
				References: this.getModel("local").getProperty("/References")
			};
			return oData;
		},

		_createHU: function() {
			if (sap.ui.getCore().byId("txtPackMat").getValue() === "") {
				sap.ui.getCore().byId("txtPackMat").setValueState("Error");
				sap.ui.getCore().byId("txtPackMat").setValueStateText(this.oBundle.getText("EnterPackMat"));
				return;
			} else {
				sap.ui.getCore().byId("txtPackMat").setValueState("None");
			}
			if (sap.ui.getCore().byId("txtHUNo").getValue() === "") {
				sap.ui.getCore().byId("txtHUNo").setValue("1");
			}

			this.showBusy();
			this.getModel().callFunction("/CreateHU", {
				urlParameters: {
					HuNo: sap.ui.getCore().byId("txtHUNo").getValue(),
					Object: this.sObject,
					ObjectKey: this.sObjectKey,
					PackagingMaterial: sap.ui.getCore().byId("txtPackMat").getValue(),
					WeightUnit: this.getModel("local").getProperty("/basic/unit/weight_unit")
				},
				success: function() {
					this._getContentAndHUTable("All");
					MessageToast.show(this.oBundle.getText("CreateHUSuccess"));
					this.oDialogNewHU.close();
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.oDialogNewHU.close();
					this.hideBusy();
				}.bind(this)
			});
		},

		_deleteHUs: function(bAll) {
			var oRequestData = this._generateDeleteHUUsecase(bAll);
			this.showBusy();
			this.getModel().create("/ShipmentQuerySet", oRequestData, {
				success: function() {
					this._getContentAndHUTable("All");
					MessageToast.show(this.oBundle.getText("DeleteHUSuccess"));
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		_generateDeleteHUUsecase: function(bAll) {
			var aSelectedHUs = [];
			if (bAll) {
				aSelectedHUs = this.byId("tableHU").getItems();
			} else {
				aSelectedHUs = this.byId("tableHU").getSelectedItems();
			}
			var aFreightUnits = [];
			for (var i = 0; i < aSelectedHUs.length; i++) {
				var oItemData = aSelectedHUs[i].getBindingContext("local").getObject();
				if (oItemData.trackingnumber === "") {
					aFreightUnits.push(oItemData);
				}
			}
			var oData = {
				shipmentid: "",
				inputids: (this.sInputIDs) ? this.sInputIDs : "",
				inputtype: (this.sInputType) ? this.sInputType : "",
				profile: (this.sProfile) ? this.sProfile : "",
				shippingstation: (this.sStation) ? this.sStation : "",
				action: "DeleteHU",
				basic: this.getModel("local").getProperty("/basic"),
				Freightunits: aFreightUnits
			};
			return oData;
		},

		_packPartial: function() {
			var oRequestData = this._generatePackPartialUsecase();
			this.showBusy();
			this.getModel().create("/ShipmentQuerySet", oRequestData, {
				success: function() {
					this._getContentAndHUTable("All");
					MessageToast.show(this.oBundle.getText("PackPartialSuccess"));
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		_generatePackPartialUsecase: function() {
			var aSelectedItems = this.byId("tableItem").getSelectedItems();
			var aContents = [];
			var sTargetHU = this.byId("tableHU").getSelectedItems()[0].getBindingContext("local").getObject().freightunitkey;
			for (var i = 0; i < aSelectedItems.length; i++) {
				aSelectedItems[i].getBindingContext("local").getObject().partialqty = aSelectedItems[i].getBindingContext("local").getObject().partialqty +
					"";
				aContents.push(aSelectedItems[i].getBindingContext("local").getObject());
			}
			var oData = {
				shipmentid: "",
				inputids: (this.sInputIDs) ? this.sInputIDs : "",
				inputtype: (this.sInputType) ? this.sInputType : "",
				profile: (this.sProfile) ? this.sProfile : "",
				shippingstation: (this.sStation) ? this.sStation : "",
				action: "PackPartial",
				targethu: sTargetHU,
				basic: this.getModel("local").getProperty("/basic"),
				Contents: aContents
			};
			return oData;
		},

		_packMaterial: function() {
			var oRequestData = this._generatePackMaterialUsecase();
			this.showBusy();
			this.getModel().create("/ShipmentQuerySet", oRequestData, {
				success: function() {
					this._getContentAndHUTable("All");
					MessageToast.show(this.oBundle.getText("PackMaterialSuccess"));
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		_generatePackMaterialUsecase: function() {
			var aSelectedItems = this.byId("tableItem").getSelectedItems();
			var aContents = [];
			var sTargetHU = this.byId("tableHU").getSelectedItems()[0].getBindingContext("local").getObject().freightunitkey;
			for (var i = 0; i < aSelectedItems.length; i++) {
				aSelectedItems[i].getBindingContext("local").getObject().partialqty = aSelectedItems[i].getBindingContext("local").getObject().partialqty +
					"";
				aContents.push(aSelectedItems[i].getBindingContext("local").getObject());
			}
			var oData = {
				shipmentid: "",
				inputids: (this.sInputIDs) ? this.sInputIDs : "",
				inputtype: (this.sInputType) ? this.sInputType : "",
				profile: (this.sProfile) ? this.sProfile : "",
				shippingstation: (this.sStation) ? this.sStation : "",
				action: "PackMaterial",
				targethu: sTargetHU,
				basic: this.getModel("local").getProperty("/basic"),
				Contents: aContents
			};
			return oData;
		},

		_unpack: function() {
			var oRequestData = this._generateUnpackUsecase();
			this.showBusy();
			this.getModel().create("/ShipmentQuerySet", oRequestData, {
				success: function() {
					this._getContentAndHUTable("All");
					MessageToast.show(this.oBundle.getText("UnpackSuccess"));
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		_generateUnpackUsecase: function() {
			var aSelectedHus = this.byId("tableHU").getSelectedItems();
			var aFreightUnits = [];
			for (var i = 0; i < aSelectedHus.length; i++) {
				aFreightUnits.push(aSelectedHus[i].getBindingContext("local").getObject());
			}
			var oData = {
				shipmentid: "",
				inputids: (this.sInputIDs) ? this.sInputIDs : "",
				inputtype: (this.sInputType) ? this.sInputType : "",
				profile: (this.sProfile) ? this.sProfile : "",
				shippingstation: (this.sStation) ? this.sStation : "",
				action: "UnpackHU",
				basic: this.getModel("local").getProperty("/basic"),
				Freightunits: aFreightUnits
			};
			return oData;
		},

		_execute: function() {
			var oRequestData = this._generateExecuteUsecase();
			var oHUTab = this.byId("tableHU");
			if (this.getModel("local").getProperty("/basic/carrier_data/mps/mps") !== "X") {
				this.oSelectedHu = oHUTab.getSelectedItem();
			}

			this.showBusy();
			this.getModel().create("/ShipmentQuerySet", oRequestData, {
				success: function(oData) {
					this._getContentAndHUTable("All");
					var oBasicData = this.getModel("local").getProperty("/basic");
					var oTracking = {};
					if (oData.ShipmentTrackingsSet.results.length > 0) {
						oTracking = oData.ShipmentTrackingsSet.results[oData.ShipmentTrackingsSet.results.length - 1];
					}
					var oPreviousShipment = {
						carrier: oBasicData.carrier_data.carrier,
						service: oBasicData.carrier_data.service,
						billing_option: oBasicData.payment.billing_option,
						shipment_date: oBasicData.date_time.shipment_date,
						weight: oTracking.totaldimweight,
						rate: oTracking.rate,
						trackingno: oTracking.mastertrack
					};
					MessageToast.show(this.oBundle.getText("ExecuteSuccess", [oTracking.mastertrack]));
					this.getModel("local").setProperty("/PreviousShipment", oPreviousShipment);
				}.bind(this),
				error: function(oError) {
					this.oSelectedHu = null;
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		_selectNextItem: function() {
			if (!this.oSelectedHu) {
				return;
			}
			var oHUTab = this.byId("tableHU");
			var aHUItems = oHUTab.getItems();
			var iSelectedIndex = -1;

			aHUItems.forEach(function(item, index) {
				if (item.getBindingContext("local").getObject().freightunitkey === this.oSelectedHu.getBindingContext("local").getObject().freightunitkey) {
					iSelectedIndex = index;
					return;
				}
			}.bind(this));
			if (iSelectedIndex < 0) {
				return;
			}
			for (var i = iSelectedIndex + 1; i < aHUItems.length; i++) {
				var oItemData = aHUItems[i].getBindingContext("local").getObject();
				if (oItemData.trackingnumber === "") {
					aHUItems[i].setSelected(true);
					return;
				}
			}
			// remove selection list
			this.oSelectedHu = null;
		},

		_generateExecuteUsecase: function() {
			var sMPSStatus = this.getModel("local").getProperty("/basic/carrier_data/mps/mps");
			var aFreightUnits = [];
			var sMPSType = this.getModel("local").getProperty("/basic/carrier_data/mps/mpstype");
			if (sMPSStatus === "X" && sMPSType === "02") {
				aFreightUnits = this.getModel("local").getProperty("/Freightunits");
			} else {
				aFreightUnits = this._getSelectedFreightUnits();
			}
			var oData = {
				shipmentid: "",
				inputids: (this.sInputIDs) ? this.sInputIDs : "",
				inputtype: (this.sInputType) ? this.sInputType : "",
				profile: (this.sProfile) ? this.sProfile : "",
				shippingstation: (this.sStation) ? this.sStation : "",
				action: "Execute",
				basic: this.getModel("local").getProperty("/basic"),
				Freightunits: aFreightUnits,
				HTS: this.getModel("local").getProperty("/HTS"),
				References: this.getModel("local").getProperty("/References"),
				ShipmentTrackingsSet: []
			};
			return oData;
		},

		_filterAllDropdowns: function() {
			/////// PARCEL TAB ///////
			// Filter the Carrier Service Dropdown based on Carrier
			var oSelectService = this.byId("selectService");
			oSelectService.getBinding("items").filter(new Filter("carrier", "EQ", this.sCarrier));

			// Filter the Package Type Dropdown based on Carrier
			this.byId("selectPackage").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));

			/////// CARRIER SPECIFIC TAB ///////
			if (this.sCarrier === "FDXE") {
				this.byId("cbCollTypeFDXE").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
				this.byId("cbSignatureOpt").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
				this.byId("cbFedExReturnService").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
			} else if (this.sCarrier === "FDXG") {
				this.byId("cbCollTypeFDXG").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
				this.byId("cbSignatureOptG").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
				this.byId("cbFedExReturnServiceG").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
			} else if (this.sCarrier === "UPS") {
				this.byId("cbCollType").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
				this.byId("cbUPSReturnService").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
				this.byId("cbDeliveryConf").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
			}

			/////// SHIP FROM/TO + SOLD TO TAB ///////
			// Filter the Shipfrom Region Dropdown based on Country
			var oShipFromStateSel = this.byId("slShipFromState");
			oShipFromStateSel.getBinding("items").filter(new Filter("Country", "EQ", this.getModel("local").getProperty(
				"/basic/partners/shipfrom/address/country")));
			oShipFromStateSel.getBinding("items").attachDataReceived(function() {
				oShipFromStateSel.setSelectedKey(this.getModel("local").getProperty("/basic/partners/shipfrom/address/state"));
			}.bind(this), this);

			// Filter the Shipto Region Dropdown based on Country
			var oShipToStateSel = this.byId("slShipToState");
			oShipToStateSel.getBinding("items").filter(new Filter("Country", "EQ", this.getModel("local").getProperty(
				"/basic/partners/shipto/address/country")));
			oShipToStateSel.getBinding("items").attachDataReceived(function() {
				oShipToStateSel.setSelectedKey(this.getModel("local").getProperty("/basic/partners/shipto/address/state"));
			}.bind(this), this);

			// Filter the Soldto Region Dropdown based on Country
			var oSoldToStateSel = this.byId("slSoldToState");
			oSoldToStateSel.getBinding("items").filter(new Filter("Country", "EQ", this.getModel("local").getProperty(
				"/basic/partners/soldto/address/country")));
			oSoldToStateSel.getBinding("items").attachDataReceived(function() {
				oSoldToStateSel.setSelectedKey(this.getModel("local").getProperty("/basic/partners/soldto/address/state"));
			}.bind(this), this);

			/////// INTERNATIONAL TAB ///////
			// Filter dropdowns based on domestic or international flag
			if (!this.getModel("local").getProperty("/basic/shipment_flags/domestic")) {
				// Filter the Importer Region Dropdown based on Country
				var oImporterStateSel = this.byId("slImporterState");
				oImporterStateSel.getBinding("items").filter(new Filter("Country", "EQ", this.getModel("local").getProperty(
					"/basic/partners/importer/address/country")));
				oImporterStateSel.getBinding("items").attachDataReceived(function() {
					oImporterStateSel.setSelectedKey(this.getModel("local").getProperty("/basic/partners/importer/address/state"));
				}.bind(this), this);

				// Carrier specific international controls
				if (this.sCarrier === "FDXE" || this.sCarrier === "FDXG") {
					// FedEx internation state filter
					var oFedExInternationalStateSel = this.byId("slFedExInternationalState");
					oFedExInternationalStateSel.getBinding("items").filter(new Filter("Country", "EQ", this.getModel("local").getProperty(
						"/basic/international/customs_broker/brokercountry")));
					oFedExInternationalStateSel.getBinding("items").attachDataReceived(function() {
						oFedExInternationalStateSel.setSelectedKey(this.getModel("local").getProperty(
							"/basic/international/customs_broker/brokerstate"));
					}.bind(this), this);

					this.byId("cbFedExTermOfSale").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
					this.byId("cbFedExTinType").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
					this.byId("cbBrokerTinType").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
					this.byId("cbStatementType").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
				} else if (this.sCarrier === "UPS") {
					this.byId("cbUPSTermOfSale").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
					this.byId("cbUPSTinType").getBinding("items").filter(new Filter("Carrier", "EQ", this.sCarrier));
				}
			}
		},

		_updateRegion: function(sId, sCountry) {
			var oRegionControl = this.byId(sId);
			oRegionControl.getBinding("items").filter(new Filter("Country", "EQ", sCountry));
			oRegionControl.setSelectedKey("");
		},

		_displayTabs: function() {
			var bDomestic = this.getModel("local").getProperty("/basic/shipment_flags/domestic");
			if (bDomestic) {
				this.byId("iconTabInternational").setVisible(false);
				this.byId("iconTabImporter").setVisible(false);
				this.byId("btnHTS").setVisible(false);
			} else {
				this.byId("iconTabInternational").setVisible(true);
				this.byId("iconTabImporter").setVisible(true);
				this.byId("btnHTS").setVisible(true);
			}
		},

		_getDefaultScaleWeight: function() {
			this.getModel().read("/xSERPERPxCDS_PROFILE", {
				filters: [
					new Filter("Profile", "EQ", this.sProfile)
				],
				success: function(oData) {
					if (oData.results.length > 0) {
						this.sDefaultUseScale = "000" + oData.results[0].UseScale;
						this.getModel("local").setProperty("/UseScale", "000" + oData.results[0].UseScale);
					}
					this.hideBusy();
				}.bind(this)
			});
		},

		_displayMessageStripHeader: function() {
			var sString = "";
			var sCarrierName = this.getModel("local").getProperty("/basic/carrier_data/CarrierName");
			var sServiceName = "";
			try {
				var aServiceList = this.getModel("local").getProperty("/ServiceList");
				for (var i = 0; i < aServiceList.length; i++) {
					if (aServiceList[i].carrier === this.sCarrier &&
						aServiceList[i].service === this.getModel("local").getProperty("/basic/carrier_data/service")) {
						sServiceName = aServiceList[i].description;
						break;
					}
				}
			} catch (exc) {
				this._oLogger.info("No Service Name");
			}
			var sShipToName = this.getModel("local").getProperty("/basic/partners/shipto/address/name1");
			var sShipToCity = this.getModel("local").getProperty("/basic/partners/shipto/address/city");
			var sShipToState = "";
			try {
				sShipToState = this.byId("slShipToState").getSelectedItem().getText();
			} catch (exc) {
				this._oLogger.info("No Ship To State");
			}
			var sShipToPost = this.getModel("local").getProperty("/basic/partners/shipto/address/postalcode");
			var sShipToCountry = "";
			try {
				sShipToCountry = this.byId("cbShipToCountry").getSelectedItem().getText();
			} catch (exc) {
				this._oLogger.info("No Ship To Country");
			}
			var sBillingOpt = "";
			try {
				sBillingOpt = this.byId("cbBillingOptionParcel").getSelectedItem().getText();
			} catch (exc) {
				this._oLogger.info("No Billing Option");
			}
			var s3PartyAcc = this.getModel("local").getProperty("/basic/payment/thirdpartyacct/account");
			var s3PartyZip = this.getModel("local").getProperty("/basic/payment/thirdpartyacct/zip");
			var s3PartyCountry = "";
			try {
				s3PartyCountry = this.byId("cbThirdPartyCountry").getSelectedItem().getText();
			} catch (exc) {
				this._oLogger.info("No 3rdParty Country");
			}
			sString = sCarrierName + " / " + sServiceName + "  -  " +
				sShipToName + " / " + sShipToCity + " / " +
				sShipToState + " / " + sShipToPost + " / " + sShipToCountry + "  -  " +
				sBillingOpt + " / " + s3PartyAcc + " / " + s3PartyZip + " / " + s3PartyCountry;
			this.byId("messageStripHeader").setText(sString);
		},

		_displaySerialList: function(oSelectedSerialMaster) {
			this.aOriginialSerialItemSet = [];
			// add more item
			if (oSelectedSerialMaster.SerialItemSet.results.length < parseInt(oSelectedSerialMaster.anzsers)) {
				var iNumberItemToAdd = parseInt(oSelectedSerialMaster.anzsers) - parseInt(oSelectedSerialMaster.anzser);

				for (var i = 0; i < iNumberItemToAdd; i++) {
					var oSerial = {
						SERNR: "",
						POSNR: oSelectedSerialMaster.posnr,
						VBELN: oSelectedSerialMaster.vbeln,
						shipmentid: ""
					};
					oSelectedSerialMaster.SerialItemSet.results.push(oSerial);
				}
			}
			this.getModel("local").setProperty("/SelectedSerial", oSelectedSerialMaster);
		},

		_generateValidateAddressUsecase: function() {
			var sActionName = "ValidateAddress";
			var oData = {
				shipmentid: "",
				inputids: (this.sInputIDs) ? this.sInputIDs : "",
				inputtype: (this.sInputType) ? this.sInputType : "",
				profile: (this.sProfile) ? this.sProfile : "",
				shippingstation: (this.sStation) ? this.sStation : "",
				action: sActionName,
				basic: this.getModel("local").getProperty("/basic"),
				Freightunits: this.getModel("local").getProperty("/Freightunits"),
				HTS: this.getModel("local").getProperty("/HTS"),
				References: this.getModel("local").getProperty("/References"),
				CarrierRates: []
			};
			return oData;
		},

		_generatePostSerialsUsecase: function() {
			var sActionName = "PostSerials";
			var aMasterList = this.getModel("local").getProperty("/MasterSerialList");
			var oData = {
				shipmentid: "",
				inputids: (this.sInputIDs) ? this.sInputIDs : "",
				inputtype: (this.sInputType) ? this.sInputType : "",
				profile: (this.sProfile) ? this.sProfile : "",
				shippingstation: (this.sStation) ? this.sStation : "",
				action: sActionName,
				SerialSet: aMasterList
			};
			return oData;
		},

		_updateShippingStatus: function() {
			var oData = this.getModel("local").getProperty("/Freightunits");

			var bCanShip = false;
			for (var i = 0; i < oData.length; i++) {
				if (oData[i].trackingnumber === "") {
					bCanShip = true;
					break;
				}
			}

			this.getModel("local").setProperty("/CanShip", bCanShip);
		},

		_getSelectedFreightUnits: function() {
			var aFreightUnits = [];
			var aSelectedHus = this.byId("tableHU").getSelectedItems();
			if (aSelectedHus.length === 0) {
				// select all item
				aFreightUnits = this.getModel("local").getProperty("/Freightunits");
			} else {
				for (var i = 0; i < aSelectedHus.length; i++) {
					aFreightUnits.push(aSelectedHus[i].getBindingContext("local").getObject());
				}
			}
			return aFreightUnits;
		},

		_getHazardous: function() {
			var oRequestData = this._generateHazardousUsecase();
			if (this.getModel("local").getProperty("/FreightunitHazmats")) {
				this.oHazmatDialog.open();
			} else {
				this.showBusy();
				this.getModel().create("/ShipmentQuerySet", oRequestData, {
					success: function(oData) {
						var aResults = oData.Freightunits.results[0].FreightunitHazmat.results;
						this.getModel("local").setProperty("/FreightunitHazmats", aResults);
						this.oHazmatDialog = Utils.getFragment("", "HazmatDialog", this);
						this.oHazmatDialog.open();
						this.hideBusy();
					}.bind(this),
					error: function(oError) {
						this._handleODataError(oError);
						this.hideBusy();
					}.bind(this)
				});
			}
		},

		_generateHazardousUsecase: function() {
			var aFreightUnits = this._getSelectedFreightUnits();
			aFreightUnits[0].FreightunitHazmat = [];
			var oData = {
				shipmentid: "",
				inputids: (this.sInputIDs) ? this.sInputIDs : "",
				inputtype: (this.sInputType) ? this.sInputType : "",
				profile: (this.sProfile) ? this.sProfile : "",
				shippingstation: (this.sStation) ? this.sStation : "",
				action: "GetHazmatTable",
				basic: this.getModel("local").getProperty("/basic"),
				Freightunits: aFreightUnits
			};
			return oData;
		},

		_validateHazmat: function() {
			var oRequestData = this._generateValidateHazmatUsecase();
			this.showBusy();
			this.getModel().create("/ShipmentQuerySet", oRequestData, {
				success: function() {
					var oBinding = this.byId(this.getView().createId("hazmatDetailContainer")).getBindingContext("local");
					var oData = oBinding.getObject();
					oData.Updated = "X";
					this.getModel("local").setProperty(oBinding.getPath(), oData);
					this.hideBusy();
					MessageToast.show(this.oBundle.getText("hazmatValidateSuccess"));
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		_generateValidateHazmatUsecase: function() {
			var aFreightUnits = this._getSelectedFreightUnits();
			var oHazmat = this.byId(this.getView().createId("hazmatDetailContainer")).getBindingContext("local").getObject();
			if (!oHazmat) {
				return null;
			}
			aFreightUnits[0].FreightunitHazmat = [oHazmat];
			var oData = {
				shipmentid: "",
				inputids: (this.sInputIDs) ? this.sInputIDs : "",
				inputtype: (this.sInputType) ? this.sInputType : "",
				profile: (this.sProfile) ? this.sProfile : "",
				shippingstation: (this.sStation) ? this.sStation : "",
				action: "ValidateHazmat",
				Freightunits: aFreightUnits
			};
			return oData;
		},

		_validateAllHazmat: function() {
			var oRequestData = this._generateValidateAllHazmatUsecase();
			this.showBusy();
			this.getModel().create("/ShipmentQuerySet", oRequestData, {
				success: function() {
					var oSelectedHu = this.byId("tableHU").getSelectedItem();
					var sPath = oSelectedHu.getBindingContext("local").getPath();
					var oHuData = oSelectedHu.getBindingContext("local").getObject();
					var aHazmat = [];
					var oItem;
					for (var i = 0; i < this.getModel("local").getProperty("/FreightunitHazmats").length; i++) {
						oItem = this.getModel("local").getProperty("/FreightunitHazmats")[i];
						if (oItem.Selected === "X" && oItem.Updated === "X") {
							aHazmat.push(oItem);
						}
					}
					oHuData.FreightunitHazmat = aHazmat;
					this.getModel("local").setProperty(sPath, oHuData);
					this.oHazmatDialog.close();
					this.hideBusy();
				}.bind(this),
				error: function(oError) {
					this._handleODataError(oError);
					this.hideBusy();
				}.bind(this)
			});
		},

		_generateValidateAllHazmatUsecase: function() {
			var aFreightUnits = this._getSelectedFreightUnits();
			// TODO: get correct selected hazmat
			var aHazmat = this.getModel("local").getProperty("/FreightunitHazmats");
			aFreightUnits[0].FreightunitHazmat = aHazmat;
			var oData = {
				shipmentid: "",
				inputids: (this.sInputIDs) ? this.sInputIDs : "",
				inputtype: (this.sInputType) ? this.sInputType : "",
				profile: (this.sProfile) ? this.sProfile : "",
				shippingstation: (this.sStation) ? this.sStation : "",
				action: "ValidateAllHazmat",
				basic: this.getModel("local").getProperty("/basic"),
				Freightunits: aFreightUnits
			};
			return oData;
		},

		_updatePackingInstr: function(oBinding) {
			var oData = oBinding.getObject();
			if (oData.Auth === "LTD.QTY") {
				oData.Packinstr = oData.Packinstr3;
			} else {
				if (oData.Airservices === "1" || oData.Airservices === "3") {
					oData.Packinstr = oData.Packinstr2;
				} else if (oData.Airservices === "2") {
					oData.Packinstr = oData.Packinstr1;
				} else if (oData.Airservices === "4") {
					oData.Packinstr = oData.Packinstr4;
				}
			}
			this.getModel("local").setProperty(oBinding.getPath(), oData);
		},
		_numOfShippedItems: function(oItemsControl) {
			var iNumOfShippedItems = 0;
			var oItemData = {};
			for (var i = 0; i < oItemsControl.length; i++) {
				oItemData = oItemsControl[i].getBindingContext("local").getObject();
				if (oItemData.trackingnumber !== "") {
					iNumOfShippedItems++;
				}
			}
			return iNumOfShippedItems;
		},

		_getShippedItems: function() {
			var aSelectedHUs = this.byId("tableHU").getItems();
			var oItemData = {};
			var aShippedItems = [];
			for (var i = 0; i < aSelectedHUs.length; i++) {
				oItemData = aSelectedHUs[i].getBindingContext("local").getObject();
				if (oItemData.trackingnumber !== "") {
					Array.prototype.push.apply(aShippedItems, oItemData.FreightunitItems.results);
				}
			}
			return aShippedItems;
		}
	});
});